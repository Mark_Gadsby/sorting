﻿using System;
using System.Diagnostics;

namespace sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] number_array = new int[7]; 
                        
            SortingAlgorithms algors = new SortingAlgorithms();

            MuddleArray(number_array);
            algors.BubbleSort(number_array);
            MuddleArray(number_array);
            algors.BubbleSort_II(number_array);
            MuddleArray(number_array);
            algors.InsertionSort(number_array);
        }

        static void MuddleArray(int [] number_array)
        {
            if (number_array.Length == 7)
            {
                number_array[0] = 9;
                number_array[1] = 5;
                number_array[2] = 4;
                number_array[3] = 15;
                number_array[4] = 3;
                number_array[5] = 8;
                number_array[6] = 11;
            }
        }

        static void RandomiseArray(int[] number_array)
        {
            Random rand = new Random();

            if (number_array.Length == 7)
            {
                for(int i = 0; i <  number_array.Length; i++)
                {
                    number_array[i] = rand.Next(0,100);
                }                
            }
        }
    }
}
